<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use App\Services\OrderServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;

class OrderController extends Controller
{
    private $orderService;

    public function __construct(OrderServiceInterface $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Collection
     */
    public function index()
    {
        $orders = $this->orderService->findAll();
        return $orders->map(function ($order) {
            return new OrderResource($order);
        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = $this->orderService->create($request);
        return new Response(new OrderResource($order));
    }

    /**
     * Display the specified resource.
     *
     * @param int $order
     * @return OrderResource|Response
     */
    public function show($order)
    {
        $order = $this->orderService->findById($order);
        if (!$order) {
            return new Response([
                'result' => 'fail',
                'message' => 'product not found'
            ]);
        }
        return new OrderResource($order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $order)
    {
        $order = $this->orderService->findById($order);
        if (!$order) {
            return new Response([
                'result' => 'fail',
                'message' => 'order not found'
            ]);
        }
        $result = $this->orderService->update($request, $order);
        if ($result) {
            return $result;
        }
        return new Response(new OrderResource($order));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $order
     * @return array
     */
    public function destroy($order)
    {
        $result = $this->orderService->delete($order);
        return ['result' => $result ? 'success' : 'fail'];
    }
}
