<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'orderId' => $this->id,
            'orderDate' => $this->date
        ];
        $orderSum = 0;
        foreach ($this->products as $product) {
            $orderItem['productName'] = $product->name;
            $orderItem['productQty'] = $product->order_items->quantity;
            $orderItem['productPrice'] = $product->order_items->price / 100;
            $orderItem['productDiscount'] = $product->order_items->discount * 100 . '%';
            $orderItem['productSum'] = $product->order_items->sum / 100;
            $orderSum += $orderItem['productSum'];
            $data['orderItems'][] = $orderItem;
        }
        $data['orderSum'] = $orderSum;
        $data['buyer']['buyerFullName'] = "{$this->buyer->name} {$this->buyer->surname}";
        $data['buyer']['buyerAddress'] = "{$this->buyer->country}, {$this->buyer->city}, {$this->buyer->addressLine}";
        $data['buyer']['phone'] = $this->buyer->phone;
        return $data;
    }
}
