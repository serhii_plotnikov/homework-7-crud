<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['buyer_id', 'date'];

//    protected $attributes = ['date'=>strtotime('+3days')];

    public function buyer()
    {
        return $this->belongsTo(Buyer::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_items')->withPivot(['quantity', 'price',
            'discount', 'sum'])->as('order_items');
    }
}
