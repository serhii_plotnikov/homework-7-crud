<?php


namespace App\Services;

use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;

class OrderService implements OrderServiceInterface
{
    public function create(Request $request): Order
    {
        if (!$request->has('date')) {
            $date = date('Y-m-d h:i:s', strtotime('+3days'));
        }
        $order = Order::create(['buyer_id' => $request->buyerId, 'date' => $date]);
        $orderItems = [];
        foreach ($request->orderItems as $orderItem) {
            $orderItems[$orderItem['productId']] = [];
            $product = Product::find($orderItem['productId']);
            $orderItems[$orderItem['productId']]['quantity'] = $orderItem['productQty'];
            $orderItems[$orderItem['productId']]['discount'] = (int)$orderItem['productDiscount'] / 100;
            $orderItems[$orderItem['productId']]['price'] = $product->price;
            $orderItems[$orderItem['productId']]['sum'] = $this->sum($orderItem['productQty'], $product->price,
                $orderItems[$orderItem['productId']]['discount']);
        }
        $order->products()->attach($orderItems);
        return $order;
    }

    public function findAll(): Collection
    {
        return Order::all();
    }

    public function findById(int $order): ?Order
    {
        return Order::find($order);
    }

    public function delete(int $order): bool
    {
        return Order::destroy($order);
    }

    public function update(Request $request, Order $order): ?Response
    {
        $productsIds = [];
        foreach ($order->products as $product) {
            $productsIds[] = $product->order_items->product_id;
        }
        $requestIds = array_map(function ($value) {
            return $value['productId'];
        }, $request->orderItems);
        if ($requestIds != array_intersect($requestIds, $productsIds)) {
            return new Response([
                'result' => 'fail',
                'message' => "productItem not found"
            ]);
        }
        foreach ($request->orderItems as $orderItem) {
            $product = Product::find($orderItem['productId']);
            $attr['quantity'] = $orderItem['productQty'];
            $attr['discount'] = (int)$orderItem['productDiscount'] / 100;
            $attr['price'] = $product->price;
            $attr['sum'] = $this->sum($orderItem['productQty'], $product->price, $attr['discount']);
            $order->products()->updateExistingPivot($orderItem['productId'], $attr);
        }
        return null;
    }

    private function sum(int $quantity, int $price, float $discount): int
    {
        return round($quantity * $price * (1 - $discount));
    }
}