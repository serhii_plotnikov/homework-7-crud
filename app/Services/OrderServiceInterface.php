<?php


namespace App\Services;


use App\Order;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;

interface OrderServiceInterface
{
    public function create(Request $request);

    public function findAll(): Collection;

    public function findById(int $order): ?Order;

    public function delete(int $order): bool;

    public function update(Request $request, Order $order):?Response;
}