<?php

use Illuminate\Database\Seeder;

class BuyersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Buyer::class, 5)->create()->each(function (\App\Buyer $buyer) {
            $buyer->orders()->saveMany(
                factory(\App\Order::class, 3)->make(['buyer_id' => null]));
        });
    }
}
