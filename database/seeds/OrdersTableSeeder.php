<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Order::all()->each(function (\App\Order $order) {
            $order->products()->attach($this->getProducts());
        });
    }

    private function getProducts(): array
    {
        $count = random_int(1, 5);
        $productIds = [];
        foreach (\App\Product::all()->random($count) as $product) {
            $data = [
                'quantity' => random_int(1, 10),
                'price' => $product->price,
                'discount' => $this->mt_randf(0, 0.6),
            ];
            $data['sum'] = round($data['quantity'] * $data['price'] * (1 - $data['discount']));
            $productIds[$product->id] = $data;
        }
        return $productIds;
    }

    private function mt_randf($min, $max)
    {
        return $min + abs($max - $min) * mt_rand(0, mt_getrandmax()) / mt_getrandmax();
    }
}
